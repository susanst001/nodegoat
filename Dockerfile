FROM atlassian/default-image:latest
MAINTAINER Sean Casey <seancasey08@gmail.com>
ARG CHECKMARX_CX_CLI_URL="http://sean-casey.com/checkmarx/CxConsolePlugin-8.60.1.zip"
RUN wget -q ${CHECKMARX_CX_CLI_URL} -O /tmp/cli.zip && \
  unzip /tmp/cli.zip -d /opt/ && \
  ln -s /opt/CxConsolePlugin* /opt/CxConsolePlugin && \
  chmod +x /opt/CxConsolePlugin/runCxConsole.sh
VOLUME /usr/src
WORKDIR /usr/src
ENV PATH /opt/CxConsolePlugin:$PATH
CMD ["/opt/CxConsolePlugin/runCxConsole.sh", "--help"]